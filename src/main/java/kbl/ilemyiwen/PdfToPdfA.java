package kbl.ilemyiwen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author atmane
 *
 * Avant l'execution, il faut installer Ghostscript via la commande:
 * ubuntu :
 * $ sudo apt update
 * $ sudo apt install ghostscript
 * 
 * Redhat : 
 * $ yum install ghostscript
 *
 * Convertir un pdf en pdf/a :o
 */
public class PdfToPdfA {
	public static void main(String[] args) throws IOException {
		String in="/home/atmane/projects/cisirh/ghostscript/input.pdf";
		String out="/home/atmane/projects/cisirh/ghostscript/output.pdf";
		String cmd = String.format("gs -dPDFA -dNOOUTERSAVE -sProcessColorModel=DeviceRGB -sDEVICE=pdfwrite -o %s -dPDFACompatibilityPolicy=1 %s",out, in);
		System.out.println("cmd => " + cmd);
		Process p = Runtime.getRuntime().exec(cmd);                                                                                                                                                     
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String s;
		while ((s = stdInput.readLine()) != null) {
		        System.out.println(s);
		}
	}
}
